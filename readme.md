# Strands - the genetic video game

This program is written in rust and built for WASM to run in the browser. We intend the [wiki](https://gitlab.com/freiguy1/strands/-/wikis/home) to host valuable information about the game's rules, designs and goals.

## Requirements

- rust & cargo (preferrably installed using `rustup`)
- any requirements which [macroquad](https://github.com/not-fl3/macroquad#build-instructions) lists for your operating system
- (optional for testing WASM build) `rustup target add wasm32-unknown-unknown`
- (optional for testing WASM build) some basic http server like `cargo install basic-http-server`

## Build & Run Targeting Local Device

- build: `cargo build`
- run: `cargo run`

note: use `--release` arg to build a release version of the executable

## Build & Run Targeting WASM

note: for this procedure, the optional WASM build requirements above are required

- build: `cargo build --target wasm32-unknown-unknown`
- run:
    - copy target/wasm32-unknown-unknown/release/strands.wasm to ./public
    - host an http server pointing at ./public
    - open a browser to [your http server]/index.html

## Other notes:

If you're having any difficulties with building and running, refer to the [macroquad repo](https://github.com/not-fl3/macroquad) for troubleshooting.

