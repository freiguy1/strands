use hecs::*;
use macroquad::prelude::*;

use crate::ai::*;
use crate::components::*;
use crate::nucleotides::*;
use crate::path::*;
use crate::pause_menu;
use crate::player::*;
use crate::settings::*;
use crate::strand::*;
use crate::systems::*;

pub enum GameMode {
    Pause,
    Play,
    GameOver,
}

pub struct Game {
    pub mov_c: MovementSettings,
    pub ecs_world: World,
    pub quit: bool,
    pub game_mode: GameMode,
    pub debug: bool,
}

impl Game {
    pub fn tick(self: &mut Self) {
        if is_key_pressed(KeyCode::D) {
            self.debug = !self.debug;
        }
        match self.game_mode {
            GameMode::Pause => {
                if is_key_pressed(KeyCode::Escape) {
                    self.game_mode = GameMode::Play;
                }
                pause_menu::draw(self);
            }
            GameMode::Play => {
                if is_key_pressed(KeyCode::Escape) {
                    self.game_mode = GameMode::Pause;
                }
                self.play_tick();
            }
            GameMode::GameOver => {
                if is_key_pressed(KeyCode::Enter) {
                    self.ecs_world = Game::init_world(&self.mov_c);
                    self.game_mode = GameMode::Play;
                }
                self.game_over_tick();
            }
        }
    }

    fn game_over_tick(&mut self) {
        for (_, (mov, pos, color, size, sides)) in self.ecs_world.query_mut::<(&Movement, &Position, &Color, &Size, &Sides)>() {
            draw(&mov, &pos.coord, color, size.0, sides.0, 3.0);
        }

        for (_, (nucleotide, position, size, _)) in self.ecs_world.query_mut::<(&Nucleotide, &Position, &Size, &Ai)>() {
            nucleotide.draw_at(&position.coord, size.0);
        }

        for (_, strand) in self.ecs_world.query_mut::<&Strand>() {
            strand.draw();
        }

        let text = &"Game Over! Press ENTER to play again.".to_string();
        let metrics = measure_text(text, None, 80, 1.0);
        draw_text(text, (screen_width() - metrics.width) / 2.0, screen_height() / 2.0, 80.0, ORANGE);
    }

    // todo some refactoring may be in order...
    fn play_tick(&mut self) {
        for (_, (mov, pos, color, size, sides)) in self.ecs_world.query_mut::<(&Movement, &Position, &Color, &Size, &Sides)>() {
            draw(&mov, &pos.coord, color, size.0, sides.0, 3.0);
        }

        for (_, (nucleotide, position, size, _)) in self.ecs_world.query_mut::<(&Nucleotide, &Position, &Size, &Ai)>() {
            nucleotide.draw_at(&position.coord, size.0);
        }

        for (_, strand) in self.ecs_world.query_mut::<&Strand>() {
            strand.draw();
        }

        for (_, health) in self.ecs_world.query_mut::<&Health>() {
            let color = if health.points <= 10 { RED } else { GREEN };
            draw_text(&health.points.to_string(), screen_width() - 200., 21.0, 40., color);
            if health.last_bonus != 0 {
                let bonus_color = if health.last_bonus < 0 { RED } else { GREEN };
                draw_text(&health.last_bonus.to_string(), screen_width() - 100., 21.0, 40., bonus_color);
            }
        }

        // Operations to run on only player
        // Using its own scope so fields get "unowned" afterward, not sure if needed
        let (player_pos, tail_pos, player_strand_len, player_entity) = {
            let (entity, (pos, mov, path, strand, _)) = self
                .ecs_world
                .query_mut::<(&Position, &mut Movement, &Path, &mut Strand, &Player)>()
                .into_iter()
                .next()
                .unwrap();

            if self.debug {
                draw_debug_info(pos, mov, &self.mov_c, path, strand);
            }

            handle_player_control_input(mov, &mut self.mov_c);

            let player_strand_len = strand.segments.len();
            let tail_pos = if player_strand_len > 0 {
                Some(strand.segments[player_strand_len - 1].position.clone())
            } else {
                None
            };
            (pos.clone(), tail_pos, player_strand_len, entity)
        };

        if self.debug {
            for (_, path) in self.ecs_world.query_mut::<&Path>() {
                path.draw(player_pos.coord);
            }
        }

        let mut captured_entity: Option<Entity> = None;
        let mut captured_base: Option<BaseType> = None;
        let mut captured_at_head: bool = false;
        for (entity, (nucleotide, pos, size, _)) in self.ecs_world.query::<(&mut Nucleotide, &Position, &Size, &Ai)>().iter() {
            if pos.coord.distance(player_pos.coord) <= size.0 * 2.0 {
                nucleotide.thickness = 3.;
                self.ecs_world
                    .query_one::<&mut Strand>(player_entity)
                    .unwrap()
                    .get()
                    .unwrap()
                    .add_segment_to_head(nucleotide);
                captured_entity = Some(entity);
                captured_base = Some(nucleotide.base);
                captured_at_head = true;
                break;
            }
            if tail_pos.as_ref().map(|tp| pos.coord.distance(*tp) < size.0 * 2.0).unwrap_or(false) {
                nucleotide.thickness = 3.;
                self.ecs_world
                    .query_one::<&mut Strand>(player_entity)
                    .unwrap()
                    .get()
                    .unwrap()
                    .add_segment_to_tail(nucleotide);
                captured_entity = Some(entity);
                captured_base = Some(nucleotide.base);
                break;
            }
        }

        // todo scoring algorithm/system
        let mut scored_points: i32 = 0;
        if let Some(captured_entity) = captured_entity {
            let (_, (strand, _)) = self
                .ecs_world
                .query_mut::<(&mut Strand, &DnaTemplate)>()
                .into_iter()
                .next()
                .unwrap();
            // todo scoring rules???
            // todo better matching sequence algorithm
            scored_points = -1;
            if player_strand_len < strand.segments.len() {
                let index = if captured_at_head {
                    strand.segments.len() - player_strand_len - 1
                } else {
                    player_strand_len
                };
                if strand.segments[index].base == captured_base.unwrap() {
                    scored_points = 4;
                    strand.segments[index].thickness = 4.;
                    strand.segments[index].size_adjustment = 2.;
                } else {
                    strand.segments[index].color = GRAY;
                    strand.segments[index].size_adjustment = -1.;
                    scored_points = -2;
                }
            }
            let _ = self.ecs_world.despawn(captured_entity);
            Game::add_free_nucleotide(&mut self.ecs_world, &self.mov_c);
        }
        for (_, (health, _)) in self.ecs_world.query_mut::<(&mut Health, &Player)>() {
            if health.last_bonus_timer > 0. {
                health.last_bonus_timer -= get_frame_time();
            } else {
                health.last_bonus = 0;
            }
            if scored_points < 1 {
                health.countdown -= get_frame_time();
                if health.countdown < 0.0 {
                    health.points -= 1;
                    health.countdown = 2.0;
                }
            }
            if scored_points != 0 {
                health.points += scored_points;
                health.last_bonus = scored_points;
                health.last_bonus_timer = 2.;
                if health.points < 0 {
                    self.game_mode = GameMode::GameOver;
                }
            }
        }
        //

        for (_, (nucleotide, pos, mov, _)) in self.ecs_world.query_mut::<(&mut Nucleotide, &Position, &mut Movement, &Ai)>() {
            update_ai_movement_nucleotide(&pos, mov, nucleotide, &self.mov_c, &player_pos);
        }

        for (_, (pos, mov)) in self.ecs_world.query_mut::<(&mut Position, &mut Movement)>() {
            update_position(pos, mov);
        }

        for (_, (mov, path, strand)) in self.ecs_world.query_mut::<(&Movement, &mut Path, &Strand)>() {
            path.update_path(
                mov.direction_degrees,
                mov.magnitude * get_frame_time(),
                (strand.segments.len() as f32) * strand.segment_size * 2.0,
            );
        }

        match self.mov_c.strand_movement {
            StrandMovementAlgorithm::FollowTheLeader => {
                for (_, (head, strand)) in self.ecs_world.query_mut::<(&Position, &mut Strand)>() {
                    strand.follow_the_leader(head);
                }
            }
            StrandMovementAlgorithm::FollowThePath => {
                for (_, (head, strand, path)) in self.ecs_world.query_mut::<(&Position, &mut Strand, &Path)>() {
                    strand.follow_the_path(head, &path.vectors);
                }
            }
        }
    }

    pub fn init() -> Self {
        let direction_granularity = 16;
        let mov_c = MovementSettings {
            direction_granularity: direction_granularity,
            min_velocity: 30.,
            max_velocity: 300.,
            turn_rate: 360.0 / (direction_granularity as f32),
            turn_delay: 0.1, // how fast allowed to turn?
            acceleration: 1.0,
            strand_movement: StrandMovementAlgorithm::FollowTheLeader,
        };

        Self {
            ecs_world: Game::init_world(&mov_c),
            mov_c,
            quit: false,
            game_mode: GameMode::Play,
            debug: false,
        }
    }

    fn init_world(mov_c: &MovementSettings) -> World {
        // ECS things
        let mut world = World::new();

        let center = Vec2::new(screen_width() / 2.0, screen_height() / 2.0);

        world.spawn((
            Position {
                // head - must always have at least 1 segment length
                coord: center.clone(),
            },
            Size(14.0),
            Sides(3),
            Strand::new(&center.clone(), 0., 0, 14.0),
            Path::new(0.0, 100),
            Movement {
                magnitude: mov_c.min_velocity,
                direction_degrees: 0.,
                remaining_turn_delay: 0.,
            },
            RED,
            Health {
                points: 100,
                countdown: 2.0, //todo configure countdown value - speed up over time?
                last_bonus: 0,
                last_bonus_timer: 0.,
            },
            Player {},
        ));

        // todo config setting for size of nucleotides
        // minimum length 9 to 51 in increments of 3 (codons)
        // todo have a marker showing which nucleotide to pick next and which ones were missed?
        world.spawn((Strand::new(&Vec2::new(15.0, 15.0), 180., 9, 14.0), DnaTemplate {}));

        for _ in 0..10 {
            // todo number of free nucleotides
            Game::add_free_nucleotide(&mut world, &mov_c)
        }
        world
    }

    fn add_free_nucleotide(world: &mut World, mov_c: &MovementSettings) {
        let direction: usize = rand::gen_range(0, mov_c.direction_granularity);
        let position = Vec2::new(rand::gen_range(0., screen_width()), rand::gen_range(0., screen_height()));
        let direction_degrees = direction as f32 * 360.0 / mov_c.direction_granularity as f32;
        world.spawn((
            Position { coord: position },
            Movement {
                magnitude: mov_c.min_velocity * rand::gen_range(1, 4) as f32,
                direction_degrees,
                remaining_turn_delay: 0.,
            },
            Size(14.0),
            Nucleotide::new_free(BaseType::random(), direction_degrees),
            Ai {},
        ));
    }
}
