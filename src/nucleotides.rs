use macroquad::prelude::*;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum BaseType {
    A,
    C,
    G,
    T,
}

impl BaseType {
    pub fn random() -> Self {
        match rand::gen_range(0, 4) {
            0 => BaseType::A,
            1 => BaseType::C,
            2 => BaseType::G,
            _ => BaseType::T,
        }
    }

    pub fn to_color(&self) -> Color {
        match self {
            BaseType::A => DARKBLUE,
            BaseType::C => MAROON,
            BaseType::G => DARKGREEN,
            BaseType::T => GOLD,
        }
    }

    pub fn to_sides(&self) -> u8 {
        match self {
            BaseType::A => 4,
            BaseType::C => 5,
            BaseType::G => 6,
            BaseType::T => 7,
        }
    }

    pub fn to_letter(&self) -> &str {
        match self {
            BaseType::A => "A",
            BaseType::C => "C",
            BaseType::G => "G",
            BaseType::T => "T",
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Nucleotide {
    pub position: Vec2,
    pub orientation: f32,
    pub color: Color,
    pub base: BaseType,
    pub thickness: f32,
    pub size_adjustment: f32,
}

//todo fix this design for the position issue?
impl Nucleotide {
    pub fn new_segment(base: BaseType, pos: Vec2, orientation: f32) -> Self {
        Self {
            position: pos,
            orientation: orientation,
            color: base.to_color(),
            base: base,
            thickness: 3.,
            size_adjustment: 0.,
        }
    }

    pub fn new_free(base: BaseType, orientation: f32) -> Self {
        Self {
            position: Vec2::new(0., 0.),
            orientation: orientation,
            color: base.to_color(),
            base: base,
            thickness: 2.,
            size_adjustment: 0.,
        }
    }

    pub fn draw(&self, size: f32) {
        self.draw_at(&self.position, size);
    }

    pub fn draw_at(&self, pos: &Vec2, size: f32) {
        let sides = self.base.to_sides();
        draw_poly_lines(
            pos.x,
            pos.y,
            sides,
            size + self.size_adjustment,
            self.orientation,
            self.thickness,
            self.color,
        );

        let text = &self.base.to_letter();
        let fontsize = (size * 2.5) as u16;
        let metrics = measure_text(text, None, fontsize, 1.0);
        let x = pos.x - metrics.width / 2.0;
        let y = pos.y - metrics.height / 2.0 + metrics.offset_y;
        draw_text(text, x, y, fontsize as f32, self.color);
    }
}
