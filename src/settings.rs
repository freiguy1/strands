use macroquad::prelude::*;

// all configurable settings / parameters could be changed
// to tune the game or change the levels over the course of the game/etc.

pub enum StrandMovementAlgorithm {
    FollowTheLeader,
    FollowThePath,
}

pub struct MovementSettings {
    pub direction_granularity: usize,
    pub min_velocity: f32,
    pub max_velocity: f32,
    pub turn_rate: f32,
    pub turn_delay: f32,
    pub acceleration: f32,
    pub strand_movement: StrandMovementAlgorithm,
}
