use macroquad::prelude::*;

use crate::components::*;
use crate::nucleotides::*;
use crate::util::*;

pub struct Strand {
    pub segments: Vec<Nucleotide>,
    pub segment_size: f32,
}

// todo methods remove segment, split strand, merge strand
// todo how to prevent memory allocations?
// todo move path tracking into strand?
impl Strand {
    pub fn new(start_pos: &Vec2, direction: f32, len: usize, size: f32) -> Self {
        let mut list = Vec::with_capacity(len);
        let mut next_pos = start_pos.clone();
        for _ in 0..len {
            next_pos.x -= to_unit_vector_x(direction) * size * 2.0;
            next_pos.y -= to_unit_vector_y(direction) * size * 2.0;
            let segment = Nucleotide::new_segment(BaseType::random(), next_pos, direction);
            list.push(segment);
            next_pos = next_pos.clone();
        }
        Self {
            segments: list,
            segment_size: size,
        }
    }

    // Each segment follows the segment directly ahead of it
    pub fn follow_the_leader(&mut self, head: &Position) {
        let mut leader_pos = &head.coord;
        for segment in self.segments.iter_mut() {
            let dx = leader_pos.x - segment.position.x;
            let dy = leader_pos.y - segment.position.y;
            let distance = (dx.powf(2.0) + dy.powf(2.0)).sqrt();
            let r = (distance - self.segment_size * 2.0) / distance;
            segment.position.x += dx * r;
            segment.position.y += dy * r;
            segment.orientation = radians_to_angle(f32::atan2(dy, dx));
            leader_pos = &segment.position;
        }
    }

    // Each segment follows the same path the head took
    pub fn follow_the_path(&mut self, head: &Position, path_vectors: &Vec<Vec2>) {
        let mut path_index = path_vectors.len() - 1;
        let mut curr_vector = path_vectors[path_index];
        let mut path_x = head.coord.x;
        let mut path_y = head.coord.y;
        let mut segment_distance = self.segment_size * 2.0;
        let mut path_distance = 0.0;
        for segment in self.segments.iter_mut() {
            while path_index > 0 && path_distance + curr_vector.y < segment_distance {
                path_x -= to_unit_vector_x(curr_vector.x) * curr_vector.y;
                path_y -= to_unit_vector_y(curr_vector.x) * curr_vector.y;
                path_distance += curr_vector.y;
                path_index -= 1;
                curr_vector = path_vectors[path_index];
            }
            let diff = segment_distance - path_distance;
            segment.position.x = path_x - to_unit_vector_x(curr_vector.x) * diff;
            segment.position.y = path_y - to_unit_vector_y(curr_vector.x) * diff;
            segment.orientation = curr_vector.x;
            segment_distance += self.segment_size * 2.0;
        }
    }

    pub fn draw(&self) {
        for segment in self.segments.iter() {
            segment.draw(self.segment_size);
        }
    }

    pub fn add_segment_to_head(&mut self, nucleotide: &mut Nucleotide) {
        self.segments.insert(0, *nucleotide);
    }

    pub fn add_segment_to_tail(&mut self, nucleotide: &mut Nucleotide) {
        self.segments.push(*nucleotide);
    }
}
