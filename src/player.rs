use macroquad::prelude::*;

use crate::components::*;
use crate::settings::*;
use crate::util::*;

// Marker struct indicating which entity is player
pub struct Player;

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Health {
    pub points: i32,
    pub countdown: f32,
    pub last_bonus: i32,
    pub last_bonus_timer: f32,
}

pub fn handle_player_control_input(mov: &mut Movement, mov_c: &mut MovementSettings) {
    let delta = get_frame_time(); // ~.016

    if is_key_down(KeyCode::Key1) {
        mov_c.acceleration = 1.0;
    }
    if is_key_down(KeyCode::Key2) {
        mov_c.acceleration = 3.0;
    }
    if is_key_down(KeyCode::Key3) {
        mov_c.acceleration = 5.0;
    }
    if is_key_down(KeyCode::Key4) {
        mov_c.acceleration = 7.0;
    }

    // set movement algorithms
    if is_key_down(KeyCode::Key5) {
        mov_c.strand_movement = StrandMovementAlgorithm::FollowTheLeader;
    }
    if is_key_down(KeyCode::Key6) {
        mov_c.strand_movement = StrandMovementAlgorithm::FollowThePath;
    }

    if mov.remaining_turn_delay == 0.0 {
        if is_key_down(KeyCode::Left) {
            mov.direction_degrees = wrap_around(0.0, 360.0, mov.direction_degrees - mov_c.turn_rate);
            mov.remaining_turn_delay = mov_c.turn_delay;
        }
        if is_key_down(KeyCode::Right) {
            mov.direction_degrees = wrap_around(0.0, 360.0, mov.direction_degrees + mov_c.turn_rate);
            mov.remaining_turn_delay = mov_c.turn_delay;
        }
    } else {
        mov.remaining_turn_delay = f32::max(0.0, mov.remaining_turn_delay - delta);
    }

    if is_key_down(KeyCode::Up) {
        mov.magnitude = f32::min(mov_c.max_velocity, mov.magnitude + mov_c.acceleration);
    }

    if is_key_down(KeyCode::Down) {
        mov.magnitude = f32::max(mov_c.min_velocity, mov.magnitude - mov_c.acceleration);
    }
}
