use macroquad::prelude::*;

use crate::util::*;

// Shared components

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Movement {
    pub direction_degrees: f32,
    pub magnitude: f32,
    pub remaining_turn_delay: f32,
}

impl Movement {
    pub fn dx(&self) -> f32 {
        to_unit_vector_x(self.direction_degrees)
    }

    pub fn dy(&self) -> f32 {
        to_unit_vector_y(self.direction_degrees)
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Position {
    pub coord: Vec2,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Size(pub f32);

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Sides(pub u8);

// marker struct for DNA template entity
pub struct DnaTemplate;
