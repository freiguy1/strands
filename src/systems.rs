use macroquad::prelude::*;

use crate::components::*;
use crate::path::*;
use crate::settings::*;
use crate::strand::*;
use crate::util::*;

// shared systems?

// todo more flexible / drawable trait?
pub fn draw(mov: &Movement, pos: &Vec2, color: &Color, size: f32, sides: u8, thickness: f32) {
    draw_poly_lines(pos.x, pos.y, sides, size, mov.direction_degrees, thickness, *color);
    //draw_circle_lines(pos.x, pos.y, size, 3.0, *color);
    //draw_circle(pos.x, pos.y, size, *color);
}

pub fn update_position(pos: &mut Position, mov: &mut Movement) {
    // put sx/sy in resources? put delta in resources?
    let delta = get_frame_time();
    let sx = screen_width();
    let sy = screen_height();

    let mut dx = mov.dx();
    let mut dy = mov.dy();

    let mut x2 = pos.coord.x + dx * delta * mov.magnitude;
    let mut y2 = pos.coord.y + dy * delta * mov.magnitude;

    let mut bounced = false;
    if (dx < 0.0 && x2 < 0.0) || (dx > 0.0 && x2 > sx) {
        dx = -dx;
        bounced = true;
    }
    if (dy < 0.0 && y2 < 30.0) || (dy > 0.0 && y2 > sy) {
        //todo calculate border position based on size of nucleotides
        dy = -dy;
        bounced = true;
    }

    if bounced {
        mov.direction_degrees = radians_to_angle(f32::atan2(dy, dx));
        if mov.direction_degrees < 0.0 {
            mov.direction_degrees += 360.0;
        }
        x2 = pos.coord.x + dx * delta * mov.magnitude;
        y2 = pos.coord.y + dy * delta * mov.magnitude;
    }

    pos.coord.x = x2;
    pos.coord.y = y2;

    //pos.coord.x = constrain(0.0, sx, pos.coord.x + delta * mov.dx() * mov.magnitude);
    //pos.coord.y = constrain(0.0, sy, pos.coord.y + delta * mov.dy() * mov.magnitude);

    //pos.coord.x = wrap_around(0.0, sx, pos.coord.x + delta * direction.x * mov.magnitude);
    //pos.coord.y = wrap_around(0.0, sy, pos.coord.y + delta * direction.y * mov.magnitude);
}

#[rustfmt::skip]
pub fn draw_debug_info(pos: &Position, mov: &Movement, mov_c: &MovementSettings, path: &Path, strand: &Strand) {
    draw_text(&get_fps().to_string(), 20.0, 120.0, 20.0, DARKGRAY);
    draw_text(&get_frame_time().to_string(), 20.0, 140.0, 20.0, DARKGRAY);
    draw_text(&screen_width().to_string(), 140.0, 120.0, 20.0, DARKGRAY);
    draw_text(&screen_height().to_string(), 140.0, 140.0, 20.0, DARKGRAY);
    draw_text(&pos.coord.x.to_string(), 220.0, 120.0, 20.0, DARKGRAY);
    draw_text(&pos.coord.y.to_string(), 220.0, 140.0, 20.0, DARKGRAY);
    draw_text(&(mov.dx() * mov.magnitude).to_string(), 320.0, 120.0, 20.0, DARKGRAY);
    draw_text(&(mov.dy() * mov.magnitude).to_string(), 320.0, 140.0, 20.0, DARKGRAY);
    draw_text(&mov_c.acceleration.to_string(), 380.0, 120.0, 20.0, DARKGRAY);
    draw_text(&mov.magnitude.to_string(), 380.0, 140.0, 20.0, DARKGRAY);
    draw_text(&mov.direction_degrees.to_string(), 460.0, 120.0, 20.0, DARKGRAY);
    draw_line(500.0, 140.0, 500.0 + mov.dx() * 40.0, 140.0 + mov.dy() * 40.0, 4.0, GRAY);
    
    draw_text(&path.vectors.len().to_string(), 560.0, 120.0, 20.0, DARKGRAY);
    draw_text(&path.total_magnitude.to_string(), 560.0, 140.0, 20.0, DARKGRAY);
    draw_text(&path.vectors[0].y.to_string(), 560.0, 160.0, 20.0, DARKGRAY);

    let len_sum : f32 = path.vectors.iter().map(|v| v.y).sum();
    draw_text(&len_sum.to_string(), 650.0, 120.0, 20.0, DARKGRAY);
    draw_text(&(strand.segment_size * (strand.segments.len() as f32) * 2.0).to_string(), 650.0, 140.0, 20.0, DARKGRAY);
}

// to display direction table
//for n in 0..direction_granularity {
//    draw_text(&directions[n].dx.to_string(), 20.0, 60.0+20.0*(n as f32), 20.0, DARKGRAY);
//    draw_text(&directions[n].dy.to_string(), 120.0, 60.0+20.0*(n as f32), 20.0, DARKGRAY);
//}
