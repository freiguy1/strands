use macroquad::prelude::*;

// todo improve this as it currently has problems:
// -If the caller's arithmetic would overflow/underflow the type, this doesn't really help.
// -If the caller can't be one-based (like coordinate system or other math that isn't a vec)
pub fn wrap_around<T: PartialOrd>(min: T, max: T, val: T) -> T {
    if val < min {
        return max;
    }
    if val > max {
        return min;
    }
    return val;
}

// pub fn constrain<T: PartialOrd>(min: T, max: T, val: T) -> T {
//     if val < min {
//         return min;
//     }
//     if val > max {
//         return max;
//     }
//     return val;
// }

pub fn to_unit_vector_y(angle: f32) -> f32 {
    f32::sin(angle_to_radians(angle))
}

pub fn to_unit_vector_x(angle: f32) -> f32 {
    f32::cos(angle_to_radians(angle))
}

pub fn radians_to_angle(radian_angle: f32) -> f32 {
    radian_angle * 180.0 / std::f32::consts::PI
}

pub fn angle_to_radians(angle: f32) -> f32 {
    angle * std::f32::consts::PI / 180.0
}

// pub fn font_height(font_size: u16) -> f32 {
//     macroquad::text::measure_text("hp", None, font_size, 1.).height
// }

// pub fn distance(pos1: &Vec2, pos2: &Vec2) -> f32 {
//     ((pos1.x - pos2.x).powf(2.0) + (pos1.y - pos2.y).powf(2.0)).sqrt()
// }
