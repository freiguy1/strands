use macroquad::prelude::*;
use macroquad::ui::{self, hash, widgets};

use crate::game::GameMode;
use crate::settings::StrandMovementAlgorithm;

const WINDOW_WIDTH: f32 = 300.;
const BUTTON_HEIGHT: f32 = 20.;
const BUTTON_MARGIN: f32 = 3.;

#[derive(Clone, Copy)]
pub enum PauseMenuButton {
    Resume,
    ToggleDebug,
    ToggleMoveMode,
}

impl PauseMenuButton {
    fn to_str(&self) -> &'static str {
        match self {
            Self::Resume => "Resume",
            Self::ToggleDebug => "Toggle Debug Mode",
            Self::ToggleMoveMode => "Toggle Movement Mode",
        }
    }

    pub fn all() -> [PauseMenuButton; 3] {
        [
            PauseMenuButton::Resume,
            PauseMenuButton::ToggleDebug,
            PauseMenuButton::ToggleMoveMode,
        ]
    }
}

pub trait PauseMenuHandler {
    fn handle_button_click(&mut self, _button: PauseMenuButton);
}

impl PauseMenuHandler for crate::game::Game {
    fn handle_button_click(&mut self, button: PauseMenuButton) {
        match button {
            PauseMenuButton::Resume => {}
            PauseMenuButton::ToggleDebug => {
                self.debug = !self.debug;
            }
            PauseMenuButton::ToggleMoveMode => {
                self.mov_c.strand_movement = match self.mov_c.strand_movement {
                    StrandMovementAlgorithm::FollowTheLeader => StrandMovementAlgorithm::FollowThePath,
                    StrandMovementAlgorithm::FollowThePath => StrandMovementAlgorithm::FollowTheLeader,
                };
            }
        }
        self.game_mode = GameMode::Play;
    }
}

// TODO: Larger Font size
// TODO: Prettify

fn draw_buttons<H: PauseMenuHandler>(ui: &mut ui::Ui, handler: &mut H) {
    for b in PauseMenuButton::all().iter() {
        let button = widgets::Button::new(b.to_str()).size(vec2(WINDOW_WIDTH - BUTTON_MARGIN - BUTTON_MARGIN, BUTTON_HEIGHT));
        if button.ui(ui) {
            handler.handle_button_click(*b);
        }
    }
}

pub fn draw<H: PauseMenuHandler>(handler: &mut H) {
    let button_count = PauseMenuButton::all().len();
    let window_height = button_count as f32 * BUTTON_HEIGHT + BUTTON_MARGIN * (button_count + 1) as f32;
    let x = (screen_width() / 2.) - (WINDOW_WIDTH / 2.);
    let y = (screen_height() / 2.) - (window_height / 2.);
    widgets::Window::new(hash!(), vec2(x, y), vec2(WINDOW_WIDTH, window_height))
        .movable(false)
        .close_button(false)
        .titlebar(false)
        .ui(&mut macroquad::ui::root_ui(), |ui| draw_buttons(ui, handler));
}
