use macroquad::prelude::*;

use crate::util::*;

pub struct Path {
    pub vectors: Vec<Vec2>,
    pub total_magnitude: f32,
}

impl Path {
    pub fn new(direction: f32, len: usize) -> Self {
        let mut list = Vec::with_capacity(len);
        list.push(Vec2::new(direction, 0.0));
        Self {
            vectors: list,
            total_magnitude: 0.0,
        }
    }

    // todo better data structure for adding and removing items at both ends of the list.
    // todo how to prevent memory allocations? available list?
    pub fn update_path(&mut self, direction: f32, magnitude: f32, len: f32) {
        let i = self.vectors.len() - 1;
        if direction == self.vectors[i].x {
            self.vectors[i].y += magnitude;
            self.total_magnitude += magnitude;
        } else {
            self.vectors.push(Vec2::new(direction, magnitude));
            self.total_magnitude += magnitude;
        }
        if self.vectors.len() > 2 && (self.total_magnitude - self.vectors[0].y) > len {
            self.total_magnitude -= self.vectors[0].y;
            self.vectors.remove(0);
        }
    }

    pub fn draw(&self, start_pos: Vec2) {
        let mut x = start_pos.x;
        let mut y = start_pos.y;
        for v in self.vectors.iter().rev() {
            let dx = to_unit_vector_x(v.x) * v.y;
            let dy = to_unit_vector_y(v.x) * v.y;
            draw_line(x, y, x - dx, y - dy, 1.0, RED);
            x -= dx;
            y -= dy;
        }
    }
}
