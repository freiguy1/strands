use macroquad::prelude::*;

mod ai;
mod components;
mod game;
mod pause_menu;
mod nucleotides;
mod path;
mod player;
mod settings;
mod strand;
mod systems;
mod util;

use game::*;

fn window_conf() -> Conf {
    Conf {
        window_title: "~~~~ Strands! ~~~~".to_owned(),
        fullscreen: true,
        ..Default::default()
    }
}

#[macroquad::main(window_conf)]
async fn main() {
    let mut game = Game::init();

    while !game.quit {
        clear_background(BLACK);

        if is_key_down(KeyCode::X) {
            game.quit = true;
        }

        game.tick();

        next_frame().await
    }
}
