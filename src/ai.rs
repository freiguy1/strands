use macroquad::prelude::*;

use crate::components::*;
use crate::nucleotides::*;
use crate::settings::*;
use crate::util::*;

// Marker struct for artificial intelligence entity
pub struct Ai;

// Some temp simple example Ai movement - randomly follow sort of?
// pub fn update_ai_movement(pos: &Position, mov: &mut Movement, mov_c: &MovementSettings, player_pos: &Position) {
//     let delta = get_frame_time(); // ~.016

//     let mut next_pos = pos.clone();
//     next_pos.coord.x = pos.coord.x + delta * mov.dx() * mov.magnitude;
//     next_pos.coord.y = pos.coord.y + delta * mov.dy() * mov.magnitude;

//     let d1 = ((pos.coord.x - player_pos.coord.x).powf(2.0) + (pos.coord.y - player_pos.coord.y).powf(2.0)).sqrt();
//     let d2 = ((next_pos.coord.x - player_pos.coord.x).powf(2.0) + (next_pos.coord.y - player_pos.coord.y).powf(2.0)).sqrt();

//     if d1 < d2 {
//         mov.direction_degrees = wrap_around(0.0, 360.0, mov.direction_degrees + mov_c.turn_rate);
//     }

//     mov.magnitude = if d1 < 200.0 { mov_c.min_velocity * 2.0 } else { mov_c.min_velocity };
// }

// something simple to just get going...
pub fn update_ai_movement_nucleotide(
    pos: &Position,
    mov: &mut Movement,
    nucleotide: &mut Nucleotide,
    mov_c: &MovementSettings,
    player_pos: &Position,
) {
    let delta = get_frame_time(); // ~.016

    let mut next_pos = pos.clone();
    next_pos.coord.x = pos.coord.x + delta * mov.dx() * mov.magnitude;
    next_pos.coord.y = pos.coord.y + delta * mov.dy() * mov.magnitude;

    let d1 = pos.coord.distance(player_pos.coord);
    let d2 = next_pos.coord.distance(player_pos.coord);

    if d1 > 400.0 && d1 < d2 {
        mov.direction_degrees = wrap_around(0.0, 360.0, mov.direction_degrees + mov_c.turn_rate);
    }

    if d1 < 100.0 && d1 > d2 {
        mov.direction_degrees = wrap_around(0.0, 360.0, mov.direction_degrees - mov_c.turn_rate);
    }
    nucleotide.orientation -= if d1 > d2 { 1. } else { -1. } * mov_c.turn_rate * get_frame_time();
}
